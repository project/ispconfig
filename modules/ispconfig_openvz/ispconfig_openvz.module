<?php

/**
 * @file
 *  Module to administer ISPConfig OpenVZ from within Drupal.
 */

/**
 *  Implements hook_ispconfig_api_functions_register().
 */
function ispconfig_openvz_ispconfig_api_functions_register() {
  $path = drupal_get_path('module', 'ispconfig_openvz');
  $file = 'ispconfig_openvz.module';

  // Standard OpenVZ add and update fields for params.
  $openvz_ip = array(
    'server_id' => '',  // server_id  (int(11))
    'ip_address' => '', // ip_address  (int(15))
    'vm_id' => '',      // vm_id  (int(11))
    'reserved' => '',   // reserved  (varchar(255))
  );
  $openvz_ostemplate = array(
    'template_name' => '',  // template_name  (varchar(255))
    'template_file' => '',  // template_file  (varchar(255))
    'server_id' => '',      // server_id  (int(11))
    'allservers' => '',     // allservers  (varchar(255))
    'active' => '',         // active  (varchar(255))
    'description' => '',    // description  (text)
  );
  $openvz_template = array(
    // template_name  (varchar(255))
    // diskspace  (int(11))
    // traffic  (int(11))
    // bandwidth  (int(11))
    // ram  (int(11))
    // ram_burst  (int(11))
    // cpu_units  (int(11))
    // cpu_num  (int(11))
    // cpu_limit  (int(11))
    // io_priority  (int(11))
    // active  (varchar(255))
    // description  (text)
    // numproc  (varchar(255))
    // numtcpsock  (varchar(255))
    // numothersock  (varchar(255))
    // vmguarpages  (varchar(255))
    // kmemsize  (varchar(255))
    // tcpsndbuf  (varchar(255))
    // tcprcvbuf  (varchar(255))
    // othersockbuf  (varchar(255))
    // dgramrcvbuf  (varchar(255))
    // oomguarpages  (varchar(255))
    // privvmpages  (varchar(255))
    // lockedpages  (varchar(255))
    // shmpages  (varchar(255))
    // physpages  (varchar(255))
    // numfile  (varchar(255))
    // avnumproc  (varchar(255))
    // numflock  (varchar(255))
    // numpty  (varchar(255))
    // numsiginfo  (varchar(255))
    // dcachesize  (varchar(255))
    // numiptent  (varchar(255))
    // swappages  (varchar(255))
    // hostname  (varchar(255))
    // nameserver  (varchar(255))
    // create_dns  (varchar(1))
    // capability  (varchar(255))
  );
  $openvz_vm = array(
    // server_id  (int(11))
    // veid  (int(10))
    // ostemplate_id  (int(11))
    // template_id  (int(11))
    // ip_address  (varchar(255))
    // hostname  (varchar(255))
    // vm_password  (varchar(255))
    // start_boot  (varchar(255))
    // active  (varchar(255))
    // active_until_date  (date)
    // description  (text)
    // diskspace  (int(11))
    // traffic  (int(11))
    // bandwidth  (int(11))
    // ram  (int(11))
    // ram_burst  (int(11))
    // cpu_units  (int(11))
    // cpu_num  (int(11))
    // cpu_limit  (int(11))
    // io_priority  (int(11))
    // nameserver  (varchar(255))
    // create_dns  (varchar(1))
    // capability  (text)
    // config  (mediumtext)
  );

  return array(
    // Native API functions.
    'openvz_get_free_ip' => array(
      'name' => 'openvz_get_free_ip',
      'parameters' => array(
        'server_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_get' => array(
          'title' => t('Get OpenVZ'),
          'description' => t('Allows retrieving OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_get_free_ip',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ip_add' => array(
      'name' => 'openvz_ip_add',
      'parameters' => array(
        'client_id' => 0,
        'params' => $openvz_ip,
      ),
      'access' => array(
        'ispconfig_openvz openvz_add' => array(
          'title' => t('Adds OpenVZ'),
          'description' => t('Allows adding a OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ip_add',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ostemplate_add' => array(
      'name' => 'openvz_ostemplate_add',
      'parameters' => array(
        'client_id' => 0,
        'params' => $openvz_ostemplate,
      ),
      'access' => array(
        'ispconfig_openvz openvz_add' => array(
          'title' => t('Adds OpenVZ'),
          'description' => t('Allows adding a OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ostemplate_add',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_template_add' => array(
      'name' => 'openvz_template_add',
      'parameters' => array(
        'client_id' => 0,
        'params' => $openvz_template,
      ),
      'access' => array(
        'ispconfig_openvz openvz_add' => array(
          'title' => t('Adds OpenVZ'),
          'description' => t('Allows adding a OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_template_add',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_vm_add' => array(
      'name' => 'openvz_vm_add',
      'parameters' => array(
        'client_id' => 0,
        'params' => $openvz_vm,
      ),
      'access' => array(
        'ispconfig_openvz openvz_add' => array(
          'title' => t('Adds OpenVZ'),
          'description' => t('Allows adding a OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_vm_add',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ip_delete' => array(
      'name' => 'openvz_ip_delete',
      'parameters' => array(
        'ip_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_delete' => array(
          'title' => t('Deletes OpenVZ'),
          'description' => t('Allows deleting OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ip_delete',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ostemplate_delete' => array(
      'name' => 'openvz_ostemplate_delete',
      'parameters' => array(
        'ostemplate_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_delete' => array(
          'title' => t('Deletes OpenVZ'),
          'description' => t('Allows deleting OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ostemplate_delete',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_template_delete' => array(
      'name' => 'openvz_template_delete',
      'parameters' => array(
        'template_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_delete' => array(
          'title' => t('Deletes OpenVZ'),
          'description' => t('Allows deleting OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_template_delete',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_vm_delete' => array(
      'name' => 'openvz_vm_delete',
      'parameters' => array(
        'vm_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_delete' => array(
          'title' => t('Deletes OpenVZ'),
          'description' => t('Allows deleting OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_vm_delete',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ip_get' => array(
      'name' => 'openvz_ip_get',
      'parameters' => array(
        'ip_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_get' => array(
          'title' => t('Get OpenVZ'),
          'description' => t('Allows retrieving OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ip_get',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ostemplate_get' => array(
      'name' => 'openvz_ostemplate_get',
      'parameters' => array(
        'ostemplate_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_get' => array(
          'title' => t('Get OpenVZ'),
          'description' => t('Allows retrieving OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ostemplate_get',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_template_get' => array(
      'name' => 'openvz_template_get',
      'parameters' => array(
        'template_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_get' => array(
          'title' => t('Get OpenVZ'),
          'description' => t('Allows retrieving OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_template_get',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_vm_get' => array(
      'name' => 'openvz_vm_get',
      'parameters' => array(
        'vm_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_get' => array(
          'title' => t('Get OpenVZ'),
          'description' => t('Allows retrieving OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_vm_get',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ip_update' => array(
      'name' => 'openvz_ip_update',
      'parameters' => array(
        'client_id' => 0,
        'ip_id' => 0,
        'params' => $openvz_ip,
      ),
      'access' => array(
        'ispconfig_openvz openvz_update' => array(
          'title' => t('Update OpenVZ'),
          'description' => t('Allows updating OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ip_update',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_ostemplate_update' => array(
      'name' => 'openvz_ostemplate_update',
      'parameters' => array(
        'client_id' => 0,
        'ostemplate_id' => 0,
        'params' => $openvz_ip,
      ),
      'access' => array(
        'ispconfig_openvz openvz_update' => array(
          'title' => t('Update OpenVZ'),
          'description' => t('Allows updating OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_ostemplate_update',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_template_update' => array(
      'name' => 'openvz_template_update',
      'parameters' => array(
        'client_id' => 0,
        'template_id' => 0,
        'params' => $openvz_ip,
      ),
      'access' => array(
        'ispconfig_openvz openvz_update' => array(
          'title' => t('Update OpenVZ'),
          'description' => t('Allows updating OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_template_update',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_vm_update' => array(
      'name' => 'openvz_vm_update',
      'parameters' => array(
        'client_id' => 0,
        'vm_id' => 0,
        'params' => $openvz_ip,
      ),
      'access' => array(
        'ispconfig_openvz openvz_update' => array(
          'title' => t('Update OpenVZ'),
          'description' => t('Allows updating OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_vm_update',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_vm_add_from_template' => array(
      'name' => 'openvz_vm_add_from_template',
      'parameters' => array(
        'client_id' => 0,
        'ostemplate_id' => 0,
        'template_id' => 0,
        'override_params' => array(),
      ),
      'access' => array(
        'ispconfig_openvz openvz_add' => array(
          'title' => t('Adds OpenVZ'),
          'description' => t('Allows adding a OpenVZ information to ISPConfig.')
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_vm_add_from_template',
      'file' => $file,
      'path' => $path,
    ),
    'openvz_vm_get_by_client' => array(
      'name' => 'openvz_vm_get_by_client',
      'parameters' => array(
        'client_id' => 0,
      ),
      'access' => array(
        'ispconfig_openvz openvz_get' => array(
          'title' => t('Get OpenVZ'),
          'description' => t('Allows retrieving OpenVZ information from ISPConfig.'),
        ),
      ),
      'native' => TRUE,
      'module' => 'ispconfig_openvz',
      'callback' => 'ispconfig_openvz_openvz_vm_get_by_client',
      'file' => $file,
      'path' => $path,
    ),
  );
}

/**
 * Implements hook_help().
 */
function ispconfig_openvz_help($path, $arg) {
  switch ($path) {
    case 'admin/help#ispconfig_openvz':
      return '<p>' . t('ISPConfig Help') . '</p>';
  }
}

//-----------------------------------------------------------------------------
// API functions of ISPConfig
//-----------------------------------------------------------------------------

/**
 * Shows the unreserved IPs for openvz virtual machines.
 *
 * Implements ISPConfig openvz_get_free_ip($session_id, $server_id = 0).
 *
 * @param int $server_id
 *  ID of the Server.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 */
function ispconfig_openvz_openvz_get_free_ip($server_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_get_free_ip',array(
    'server_id' => $server_id,
  ), $session_id, $show_errors);
}

/**
 * Adds a new openvz ip.
 *
 * Implements ISPConfig openvz_ip_add($session_id, $client_id, $params).
 *
 * @param $client_id
 *  Id of the Client.
 * @param $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  ID of newly added resource or NULL on errors.
 */
function ispconfig_openvz_openvz_ip_add($client_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ip_add',array(
    'client_id' => $client_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Adds a new openvz OS template.
 *
 * Implements ISPConfig openvz_ostemplate_add($session_id, $client_id, $params).
 *
 * @param $client_id
 *  Id of the Client.
 * @param $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  ID of newly added resource or NULL on errors.
 */
function ispconfig_openvz_openvz_ostemplate_add($client_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ostemplate_add',array(
    'client_id' => $client_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Adds a new openvz template.
 *
 * Implements ISPConfig openvz_template_add($session_id, $client_id, $params).
 *
 * @param $client_id
 *  Id of the Client.
 * @param $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  ID of newly added resource or NULL on errors.
 */
function ispconfig_openvz_openvz_template_add($client_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_template_add',array(
    'client_id' => $client_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Adds a new openvz vm.
 *
 * Implements ISPConfig openvz_vm_add($session_id, $client_id, $params).
 *
 * @param $client_id
 *  Id of the Client.
 * @param array $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  ID of newly added resource or NULL on errors.
 */
function ispconfig_openvz_openvz_vm_add($client_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_vm_add',array(
    'client_id' => $client_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Updates an openvz ip.
 *
 * Implements ISPConfig openvz_ip_update($session_id, $client_id, $ip_id, $params).
 *
 * @param int $client_id
 *  ID of the Client.
 * @param int $ip_id
 *  ID of the IP.
 * @param array $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of affected rows or NULL on error.
 */
function ispconfig_openvz_openvz_ip_update($client_id, $ip_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ip_update',array(
    'client_id' => $client_id,
    'ip_id' => $ip_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Updates an openvz ip.
 *
 * Implements ISPConfig openvz_ip_update($session_id, $client_id, $ip_id, $params).
 *
 * @param int $client_id
 *  ID of the Client.
 * @param int $ostemplate_id
 *  ID of the OSTemplate.
 * @param array $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of affected rows or NULL on error.
 */
function ispconfig_openvz_openvz_ostemplate_update($client_id, $ostemplate_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ostemplate_update',array(
    'client_id' => $client_id,
    'ostemplate_id' => $ostemplate_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Updates an openvz template.
 *
 * Implements ISPConfig openvz_template_update($session_id, $client_id, $template_id, $params).
 *
 * @param int $client_id
 *  ID of the Client.
 * @param int $template_id
 *  ID of the Template.
 * @param array $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of affected rows or NULL on error.
 */
function ispconfig_openvz_openvz_template_update($client_id, $template_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_template_update',array(
    'client_id' => $client_id,
    'template_id' => $template_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Updates an openvz vm.
 *
 * Implements ISPConfig openvz_vm_update($session_id, $client_id, $vm_id, $params).
 *
 * @param int $client_id
 *  ID of the Client.
 * @param int $vm_id
 *  ID of the VM.
 * @param array $params
 *  Associative array containing the dns information.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of affected rows or NULL on error.
 */
function ispconfig_openvz_openvz_vm_update($client_id, $vm_id, $params, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_vm_update',array(
    'client_id' => $client_id,
    'vm_id' => $vm_id,
    'params' => $params,
  ), $session_id, $show_errors);
}

/**
 * Deletes an openvz ip.
 *
 * Implements ISPConfig openvz_ip_delete($session_id, $ip_id).
 *
 * @param $ip_id
 *  ID of the IP.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of deleted records or NULL on error.
 */
function ispconfig_openvz_openvz_ip_delete($ip_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ip_delete',array(
    'ip_id' => $ip_id,
  ), $session_id, $show_errors);
}

/**
 * Deletes an openvz OS template.
 *
 * Implements ISPConfig openvz_ostemplate_delete($session_id, $ostemplate_id).
 *
 * @param $ostemplate_id
 *  ID of the OS Template.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of deleted records or NULL on error.
 */
function ispconfig_openvz_openvz_ostemplate_delete($ostemplate_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ostemplate_delete',array(
    'ostemplate_id' => $ostemplate_id,
  ), $session_id, $show_errors);
}

/**
 * Deletes an openvz template.
 *
 * Implements ISPConfig openvz_template_delete($session_id, $template_id).
 *
 * @param $template_id
 *  ID of the Template.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of deleted records or NULL on error.
 */
function ispconfig_openvz_openvz_template_delete($template_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_template_delete',array(
    'template_id' => $template_id,
  ), $session_id, $show_errors);
}

/**
 * Deletes an openvz vm.
 *
 * Implements ISPConfig openvz_vm_delete($session_id, $vm_id).
 *
 * @param $vm_id
 *  ID of the VM.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Number of deleted records or NULL on error.
 */
function ispconfig_openvz_openvz_vm_delete($vm_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_vm_delete',array(
    'vm_id' => $vm_id,
  ), $session_id, $show_errors);
}

/**
 * Retrieves information about an openvz ip.
 *
 * Implements ISPConfig openvz_ip_get($session_id, $ip_id).
 *
 * @param $ip_id
 *  ID of the IP.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  All fields and values of chosen resource or NULL on error.
 */
function ispconfig_openvz_openvz_ip_get($ip_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ip_get',array(
    'ip_id' => $ip_id,
  ), $session_id, $show_errors);
}

/**
 * Retrieves information about an openvz OS template.
 *
 * Implements ISPConfig openvz_ostemplate_get($session_id, $ostemplate_id).
 *
 * @param $ostemplate_id
 *  ID of the OSTemplate.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  All fields and values of chosen resource or NULL on error.
 */
function ispconfig_openvz_openvz_ostemplate_get($ostemplate_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_ostemplate_get',array(
    'ostemplate_id' => $ostemplate_id,
  ), $session_id, $show_errors);
}

/**
 * Retrieves information about an openvz template.
 *
 * Implements ISPConfig openvz_template_get($session_id, $template_id)
 *
 * @param $template_id
 *  ID of the Template.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  All fields and values of chosen resource or NULL on error.
 */
function ispconfig_openvz_openvz_template_get($template_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_template_get',array(
    'template_id' => $template_id,
  ), $session_id, $show_errors);
}

/**
 * Retrieves information about an openvz vm.
 *
 * Implements ISPConfig openvz_vm_get($session_id, $vm_id).
 *
 * @param $vm_id
 *  ID of the VM.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  All fields and values of chosen resource or NULL on error.
 */
function ispconfig_openvz_openvz_vm_get($vm_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_vm_get',array(
    'vm_id' => $vm_id,
  ), $session_id, $show_errors);
}

/**
 * Shows all of a client's vms.
 *
 * Implements ISPConfig openvz_vm_get_by_client($session_id, $client_id).
 * @param $client_id
 *  Id of the Client.
 * @param string $session_id (optional)
 *  ID of an existing session with ISPConfig. If not set, a new session
 *  will be opened for this request and closed afterwards.
 * @param bool $show_errors (optional)
 *  Whether to show errors (true) or not (false). Defaults to true.
 * @return mixed
 *  Arrays of parameters of target client vm or NULL on errors.
 */
function ispconfig_openvz_openvz_vm_get_by_client($client_id, $session_id = '', $show_errors = TRUE) {
  // Call the ISPConfig Core Module.
  return ispconfig_api_execute('openvz_vm_get_by_client',array(
    'client_id' => $client_id,
  ), $session_id, $show_errors);
}
